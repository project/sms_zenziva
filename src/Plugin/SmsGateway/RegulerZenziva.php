<?php

namespace Drupal\sms_zenziva\Plugin\SmsGateway;

use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResultStatus;
use GuzzleHttp\Client;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SmsGateway(
 *   id = "zenziva",
 *   label = @Translation("Reguler Zenziva"),
 *   outgoing_message_max_recipients = 1,
 *   reports_push = TRUE,
 *   incoming = TRUE,
 *   incoming_route = TRUE
 * )
 */
class RegulerZenziva extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  private const REPORT_ERRORS = [
    1 => SmsMessageReportStatus::INVALID_RECIPIENT,
    6 => SmsMessageReportStatus::CONTENT_INVALID,
  ];

  private const RESULT_ERRORS = [
    5 => SmsMessageResultStatus::ACCOUNT_ERROR,
    89 => SmsMessageResultStatus::EXCESSIVE_REQUESTS,
    99 => SmsMessageResultStatus::NO_CREDIT,
  ];

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $client;
    $this->setLoggerFactory($logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): RegulerZenziva {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'userkey' => '',
      'passkey' => '',
      'url' => 'https://reguler.zenziva.net/apps/smsapi.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['zenziva'] = [
      '#type' => 'details',
      '#title' => $this->t('Zenziva'),
      '#open' => TRUE,
    ];

    $form['zenziva']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('API keys can be found at <a href="@url">@url</a>.', [
        '@url' => 'https://reguler.zenziva.net/apps/api.php',
      ]),
    ];

    $form['zenziva']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $config['url'],
      '#required' => TRUE,
    ];

    $form['zenziva']['userkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Userkey'),
      '#default_value' => $config['userkey'],
      '#placeholder' => 'XXXXXX',
      '#required' => TRUE,
    ];

    $form['zenziva']['passkey'] = [
      '#type' => 'password',
      '#title' => $this->t('Passkey'),
      '#default_value' => $config['passkey'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['userkey'] = trim($form_state->getValue('userkey'));
    $this->configuration['passkey'] = trim($form_state->getValue('passkey'));
    $this->configuration['url'] = trim($form_state->getValue('url'));
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message): SmsMessageResult {
    $config = $this->getConfiguration();
    $recipient = $sms_message->getRecipients()[0];
    $message = $sms_message->getMessage();

    $message_result = new SmsMessageResult();
    $delivery_report = new SmsDeliveryReport();

    $delivery_report->setRecipient($recipient);
    $delivery_report->setStatus(SmsMessageReportStatus::QUEUED);
    $delivery_report->setMessageId($sms_message->getUuid());

    try {
      // Hits Zenziva API to send Message.
      $response = $this->httpClient->request('GET', $config['url'], [
        'query' => [
          'userkey' => $config['userkey'],
          'passkey' => $config['passkey'],
          'nohp' => $recipient,
          'pesan' => $message,
        ],
      ]);

      // Processes response content as XMLElement.
      $response_content = new \SimpleXMLElement($response->getBody()->getContents());

      // Gets response status from Zenziva.
      $res_status = (int) $response_content->message[0]->status->__toString();

      if ($res_status === 0) {
        $delivery_report->setStatus(SmsMessageReportStatus::DELIVERED);
        $delivery_report->setStatusMessage($this->t('Message sent to: @recipient', ['@recipient' => $recipient]));
        $message_result->addReport($delivery_report);
        return $message_result;
      }

      $report_status = !in_array($res_status, self::RESULT_ERRORS) ? SmsMessageReportStatus::ERROR : self::RESULT_ERRORS[$res_status];
      $result_status = !in_array($res_status, self::REPORT_ERRORS) ? SmsMessageResultStatus::ERROR : self::REPORT_ERRORS[$res_status];

      $delivery_report->setStatus($report_status);
      $delivery_report->setStatusMessage($delivery_report);

      $this->setLogMessage($delivery_report, $message);
    }
    catch (GuzzleException $exception) {
      $delivery_report->setStatus(SmsMessageReportStatus::ERROR);
      $delivery_report->setStatusMessage(SmsMessageReportStatus::ERROR);

      $result_status = SmsMessageResultStatus::ERROR;

      $this->setLogMessage($delivery_report, $message);
    }

    $message_result->addReport($delivery_report);
    $message_result->setError($result_status);

    return $message_result;
  }

  /**
   * Sets log message if error occured.
   *
   * @param \Drupal\sms\Message\SmsDeliveryReport $report
   *   The report.
   * @param string $message
   *   The message.
   */
  protected function setLogMessage(SmsDeliveryReport $report, $message): void {
    $this->getLogger('sms_zenziva')
      ->error($this->t('There is a problem when sending message: @message to @number, because of @error.', [
        '@message' => $message,
        '@number' => $report->getRecipient(),
        '@error' => $report->getStatusMessage(),
      ]));
  }

}
